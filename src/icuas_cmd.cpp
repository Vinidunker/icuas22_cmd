#include <icuas_cmd/icuas_cmd.hpp>

DroneCMD::DroneCMD(ros::NodeHandle & nh): nh_(nh)
{
    cmd_pos_pub = nh_.advertise<geometry_msgs::PoseStamped>
            ("/red/tracker/input_pose", 10);
    
    cmd_traj_pub = nh_.advertise<trajectory_msgs::MultiDOFJointTrajectory>
            ("/red/tracker/input_trajectory", 10);

    cmd_drone_pub = nh_.advertise<trajectory_msgs::MultiDOFJointTrajectoryPoint>
            ("/red/position_hold/trajectory", 10);

    ballDrop_pub = nh_.advertise<std_msgs::Float32>
            ("/red/uav_magnet/gain", 10);

    droneMsg_sub = nh_.subscribe<state_machine::CmdMsg>
            ("/drone/cmd", 10, &DroneCMD::droneMsgCallback, this);
}

void DroneCMD::droneMsgCallback(const state_machine::CmdMsg::ConstPtr& msg)
{
    drone_msg_ = *msg;

    if(msg->cmd_type.data == DRONE_MODES::position)
    {
        ROS_INFO_THROTTLE(3,"Commanding Position!");
        positionCmd();
    }
    else if(msg->cmd_type.data == DRONE_MODES::trajectory)
    {
        ROS_INFO_THROTTLE(3,"Commanding Trajectory!");
        trajectoryCmd();
    }
}

void DroneCMD::positionCmd()
{
    //Position Command
    geometry_msgs::PoseStamped position_goal;
    position_goal.pose.position.x = drone_msg_.position.position.x;
    position_goal.pose.position.y = drone_msg_.position.position.y;
    position_goal.pose.position.z = drone_msg_.position.position.z;

    cmd_pos_pub.publish(position_goal);
}

void DroneCMD::trajectoryCmd()
{
    // geometry_msgs::Transform pos1;
    // geometry_msgs::Transform pos2;
    // geometry_msgs::Transform pos4;
    // geometry_msgs::Transform pos5;
    // geometry_msgs::Transform pos6;

    // geometry_msgs::PoseStamped pos3;
    // //ROS_INFO("Assinging pos1!");
    // pos1.translation.x = 0;
    // pos1.translation.y = 1;
    // pos1.translation.z = 5;

    // pos2.translation.x = 0;
    // pos2.translation.y = 3;
    // pos2.translation.z = 7;

    // pos4.translation.x = 0;
    // pos4.translation.y = 5;
    // pos4.translation.z = 7;
 
    // pos5.translation.x = 0;
    // pos5.translation.y = 9;
    // pos5.translation.z = 7;

    // pos6.translation.x = 10;
    // pos6.translation.y = 1;
    // pos6.translation.z = 5;

    // pos3.pose.position.x = 0;
    // pos3.pose.position.y = 0;
    // pos3.pose.position.z = 5;

    // trajectory_.push_back(pos1);
    // trajectory_.push_back(pos2);
    // trajectory_.push_back(pos4);
    // trajectory_.push_back(pos5);
    // trajectory_.push_back(pos6);
    
    
    // msgCmd_.transforms.push_back(pos1);
    // //msgCmd_.transforms.push_back(pos2);

    // if (x == 1)
    // {
    //     ROS_INFO("Assinging pos1!");
    //     //drone_cmd_pub.publish(msgCmd_);
    //     cmd_pos_pub.publish(pos3);
    //     x = 0;
    // }
}

int main(int argc, char **argv)
{

    // initialize ROS and the node
    ros::init(argc, argv, "DroneCMD");
    ros::NodeHandle nh;

    DroneCMD drone_cmd(nh);

    ros::Rate rate(50.0);
    
    ros::spinOnce();


    // wait for FCU connection
    while(ros::ok()){

        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}