#include <iostream>
#include <string>
#include <ros/ros.h>
#include <trajectory_msgs/MultiDOFJointTrajectoryPoint.h>
#include <trajectory_msgs/MultiDOFJointTrajectory.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Transform.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <state_machine/state_machine.hpp>
#include <state_machine/CmdMsg.h>

class DroneCMD
{

    private:

        ros::NodeHandle nh_;

    public:

        DroneCMD(ros::NodeHandle & nh);

        ros::Publisher cmd_pos_pub;
        ros::Publisher cmd_traj_pub;
        ros::Publisher cmd_drone_pub;
        ros::Publisher ballDrop_pub;

        ros::Subscriber droneMsg_sub;
        

        geometry_msgs::Transform transform;

        trajectory_msgs::MultiDOFJointTrajectoryPoint msgCmd_;

        std::vector<geometry_msgs::Transform> trajectory_;

        state_machine::CmdMsg drone_msg_;

        void droneMsgCallback(const state_machine::CmdMsg::ConstPtr& msg);

        
        int x = 1;


        void positionCmd();
        void trajectoryCmd();
};
